#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_8035E84C(Actor *this);

/* .data */
extern ActorAnimationInfo D_80372F10[];

extern ActorInfo D_80372F50 = { 
    0x4, 0x5, 0x350, 
    0x1, D_80372F10, 
    func_8035E84C, func_80326224, func_80325888, 
    { 0x7, 0xD0}, 0, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};

/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035DFD0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E008.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E0D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E150.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E1B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E2A8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E360.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E44C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E540.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E634.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E724.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E750.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E810.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_D7040/func_8035E84C.s")
