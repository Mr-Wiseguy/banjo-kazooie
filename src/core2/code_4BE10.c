#include <ultra64.h>
#include "functions.h"
#include "variables.h"

void func_802D2DA0(Actor *this);

/* .data */
extern ActorInfo D_803675F0 = {
    0x16, 0x6A, 0x0,
    0x1, NULL, 
    func_802D2DA0, func_80326244, func_80325340, 
    { 0x0, 0x0}, 0, 0.0f, { 0x0, 0x0, 0x0, 0x0}
};


/* .code */
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_4BE10/func_802D2DA0.s")
