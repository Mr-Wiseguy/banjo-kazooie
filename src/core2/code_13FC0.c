#include <ultra64.h>
#include "functions.h"
#include "variables.h"

ParticleEmitter * func_802EDD8C(f32[3], f32, f32);


f32 func_8029AF50(f32 arg0, f32 arg1, f32 arg2){
    return (arg0 - arg1)/(arg2 - arg1);
}

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029AF68.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B0C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B11C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B174.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B2D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B2DC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B2E8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B2F4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B300.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B30C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B318.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B324.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B33C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B370.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B37C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B388.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B39C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B3B0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B41C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B458.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B504.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B564.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B56C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B5EC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B62C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B6F0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B73C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B85C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B890.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B930.s")

ParticleEmitter *func_8029B950(f32 pos[3],f32 arg1){
    return func_802EDD8C(pos, arg1, func_80294500());
}

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B984.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B9C0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029B9FC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BA44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BA80.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BAF0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BC60.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BCAC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BCF8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BD44.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BD90.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BDBC.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BDE8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BE10.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BE5C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BE88.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BED4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BF00.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BF4C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029BF78.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C0D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C22C.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C304.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C348.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C3E8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C4E4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C5E8.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C608.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C674.s")

extern f32 D_80374D98;

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C6D0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C748.s")

enum bs_e func_8029C780(void){
    if(button_held(BUTTON_Z) && can_flip())
        return BS_BFLIP;

    if(func_802933C0(2))
        return BS_5_JUMP;

    if(func_802933C0(1))
        return BS_23_FLY_ENTER;

    return BS_5_JUMP;
}

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C7F4.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C834.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C848.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029C984.s")

s32 func_8029C9C0(s32 arg0){
    if(func_802933C0(0xF))
        return arg0;
    
    if(button_pressed(BUTTON_A))
        arg0 = func_8029C780();

    if(button_pressed(BUTTON_B) && can_claw())
        arg0 = BS_CLAW;

    if(button_held(BUTTON_Z) && should_beak_barge())
        arg0 = BS_BBARGE;

    if(func_80294F78())
        arg0 = func_802926C0();
    
    if(player_isSliding())
        arg0  = BS_SLIDE;

    return arg0;
}

s32 func_8029CA94(s32 arg0){
    if(func_802933C0(0x19))
        arg0 = func_80292738();
    
    if(func_802933C0(0x1A))
        arg0 = (player_getTransformation() == TRANSFORM_6_BEE) ? 0x46 : BS_JIG_NOTEDOOR;

    if(func_802933C0(0xE))
        arg0 = BS_LONGLEG_ENTER;

    if(func_802933C0(0x10))
        arg0 = BS_BTROT_ENTER;

    if(func_802933C0(0x6))
        arg0 = 0x53;

    if(func_802933C0(0x7))
        arg0 = BS_JIG_JIGGY;

    if(func_802933C0(0x14))
        arg0 = (player_getTransformation() == TRANSFORM_4_WALRUS) ? 0x80 : 0x53;
    
    func_802933FC(0xF);

    return arg0;
}

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029CB84.s")

void func_8029CBC4(void);
#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029CBC4.s")

void func_8029CBF4(void){
    if(item_getCount(ITEM_E_JIGGY) == 10){
        if( jiggyscore_total() == 100 && func_8031FF1C(BKPROG_FC_DEFEAT_GRUNTY)){
            timedFunc_set_3(D_80374D98, func_802E4078, MAP_95_CS_END_ALL_100, 0, 1);
        }//L8029CC58

        timedFunc_set_0(4.0f, func_8029CBC4);
        func_8025A6EC(COMUSIC_42_NOTEDOOR_OPENING_FANFARE, -1);
    }//L8029CC7C
    else{
        if( jiggyscore_total() == 100 && func_8031FF1C(BKPROG_FC_DEFEAT_GRUNTY)){
            func_802E4078(MAP_95_CS_END_ALL_100, 0, 1);
        }
        func_8029CBC4();
    }

}

void func_8029CCC4(void){
    if(func_802933D0(7)) return;
    if( func_802933C0(0xF)
        && func_802933D0(6)
        && func_802933D0(20)
    ){
        func_802933FC(0xF);
    }
    func_802933FC(7);
    func_802B0CD8();
    item_inc(ITEM_E_JIGGY);
    if(jiggyscore_total() == 100 && func_8031FF1C(BKPROG_FC_DEFEAT_GRUNTY)){
        func_8028F918(2);
    }
    func_8024BD08(0);
    func_8025A55C(0, 4000, 0xC);
    func_8025A6EC(COMUSIC_D_JINGLE_JIGGY_COLLECTED, -1);
    timedFunc_set_0(4.0f, func_8029CBF4);
}

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029CDA0.s")

#pragma GLOBAL_ASM("asm/nonmatchings/core2/code_13FC0/func_8029CDC0.s")
